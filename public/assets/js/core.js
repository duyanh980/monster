var Rocket = function() {

    var handleInit = function() {
        if($(window).width()>=980) {
            $('.page-sidebar').height($(window).height()-60);
        } else {
            $('.page-sidebar').height('100%');
        }
        $('#validate').validationEngine();
        $("#menu-collapse").click(function () {
            if ($('.page-sidebar').hasClass('mini')) {
                $('.page-sidebar').removeClass('mini');
                $('.page-content').removeClass('condensed-layout');
                $('.footer-widget').show();
            } else {
                $('.page-sidebar').addClass('mini');
                $('.page-content').addClass('condensed-layout');
                $('.footer-widget').hide();
                calculateHeight();
            }
        });
        $(".select2").select2();

        $('.input-append.date').datepicker({
            autoclose: true,
            todayHighlight: true
        });


        $(".inside").children('input').blur(function () {
            $(this).parent().children('.add-on').removeClass('input-focus');
        });
        $(".inside").children('input').focus(function () {
            $(this).parent().children('.add-on').addClass('input-focus');
        });
        $(".input-group.transparent").children('input').blur(function () {
            $(this).parent().children('.input-group-addon').removeClass('input-focus');
        });
        $(".input-group.transparent").children('input').focus(function () {
            $(this).parent().children('.input-group-addon').addClass('input-focus');
        });
        $(".bootstrap-tagsinput input").blur(function () {
            $(this).parent().removeClass('input-focus');
        });
        $(".bootstrap-tagsinput input").focus(function () {
            $(this).parent().addClass('input-focus');
        });
        $(".simple-chat-popup").click(function () {
            $(this).addClass('hide');
            $('#chat-message-count').addClass('hide');
        });
        setTimeout(function () {
            $('#chat-message-count').removeClass('hide');
            $('#chat-message-count').addClass('animated bounceIn');
            $('.simple-chat-popup').removeClass('hide');
            $('.simple-chat-popup').addClass('animated fadeIn');
        }, 5000);
        setTimeout(function () {
            $('.simple-chat-popup').addClass('hide');
            $('.simple-chat-popup').removeClass('animated fadeIn');
            $('.simple-chat-popup').addClass('animated fadeOut');
        }, 8000);
        jQuery('.page-sidebar li > a').on('click', function (e) {
            if ($(this).next().hasClass('sub-menu') === false) {
                return;
            }
            var parent = $(this).parent().parent();
            parent.children('li.open').children('a').children('.arrow').removeClass('open');
            parent.children('li.open').children('a').children('.arrow').removeClass('active');
            parent.children('li.open').children('.sub-menu').slideUp(200);
            parent.children('li').removeClass('open');
            var sub = jQuery(this).next();
            if (sub.is(":visible")) {
                jQuery('.arrow', jQuery(this)).removeClass("open");
                jQuery(this).parent().removeClass("active");
                sub.slideUp(200);
            } else {
                jQuery('.arrow', jQuery(this)).addClass("open");
                jQuery(this).parent().addClass("open");
                sub.slideDown(200);
            }
            e.preventDefault();
        });

        if ($('.page-sidebar').hasClass('mini')) {
            var elem = jQuery('.page-sidebar ul');
            elem.children('li.open').children('a').children('.arrow').removeClass('open');
            elem.children('li.open').children('a').children('.arrow').removeClass('active');
            elem.children('li.open').children('.sub-menu').slideUp(200);
            elem.children('li').removeClass('open');
        }
        $('[data-height-adjust="true"]').each(function () {
            var h = $(this).attr('data-elem-height');
            $(this).css("min-height", h);
            $(this).css('background-image', 'url(' + $(this).attr("data-background-image") + ')');
            $(this).css('background-repeat', 'no-repeat');
            if ($(this).attr('data-background-image')) {
            }
        });

        $('.tip').tooltip();
        $('.horizontal-menu .bar-inner > ul > li').on('click', function () {
            $(this).toggleClass('open').siblings().removeClass('open');
        });
        if ($('body').hasClass('horizontal-menu')) {
            $('.content').on('click', function () {
                $('.horizontal-menu .bar-inner > ul > li').removeClass('open');
            });
        }
        if ($.fn.lazyload) {
            $("img.lazy").lazyload({effect: "fadeIn"});
        }
        $('.user-info .collapse').on('click', function () {
            jQuery(this).parents(".user-info ").stop().slideToggle(400, "swing");
        });
        $('.panel-group').on('hidden.bs.collapse', function (e) {
            $(this).find('.panel-heading').not($(e.target)).addClass('collapsed');
        });
        $(window).setBreakpoints({distinct: true, breakpoints: [320, 480,768, 1024]});

        function closeAndRestSider() {
            if ($('#main-menu').attr('data-inner-menu') == '1') {
                $('#main-menu').addClass("mini");
                $('#main-menu').removeClass("left");
            } else {
                $('#main-menu').removeClass("left");
            }
        }

        $('#horizontal-menu-toggle').click(function () {
            if ($('body').hasClass('breakpoint-480') || $('body').hasClass('breakpoint-320')) {
                $('.bar').slideToggle(200, "linear");
            }
        });
        $('.scroller').each(function () {
            var h = $(this).attr('data-height');
            $(this).mCustomScrollbar();
            if (h != null || h != "") {
                if ($(this).parent('.scroll-wrapper').length > 0)
                    $(this).parent().css('max-height', h); else
                    $(this).css('max-height', h);
            }
        });
        $('.dropdown-toggle').click(function () {
            $("img").trigger("unveil");
        });

        $('table th .checkall').on('click', function () {
            if ($(this).is(':checked')) {
                $(this).closest('table').find(':checkbox').attr('checked', true);
                $(this).closest('table').find('tr').addClass('row_selected');
            } else {
                $(this).closest('table').find(':checkbox').attr('checked', false);
                $(this).closest('table').find('tr').removeClass('row_selected');
            }
        });
        if (!jQuery().sortable) {
            return;
        }
        $(".sortable").sortable({connectWith: '.sortable', iframeFix: false, items: 'div.grid', opacity: 0.8, helper: 'original', revert: true, forceHelperSize: true, placeholder: 'sortable-box-placeholder round-all', forcePlaceholderSize: true, tolerance: 'pointer'});
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });
        $('.scrollup').click(function () {
            $("html, body").animate({scrollTop: 0}, 700);
            return false;
        });
        $("img").unveil();



        function toggleMainMenu() {
            var timer;
            if ($('body').hasClass('open-menu-left')) {
                $('body').removeClass('open-menu-left');
                timer = setTimeout(function () {
                    $('.page-sidebar').removeClass('visible');
                }, 300);
            }
            else {
                clearTimeout(timer);
                $('.page-sidebar').addClass('visible');
                setTimeout(function () {
                    $('body').addClass('open-menu-left');
                }, 50);
            }
        }
    };

    var handleInitSidebar = function() {
        $('.page-sidebar').mCustomScrollbar();
    }

    var handleInitSidebarLayout = function() {


        $(window).setBreakpoints({distinct: true, breakpoints: [320, 480, 768, 1024]});
        $(window).bind('enterBreakpoint320', function () {
            $('#main-menu-toggle-wrapper').show();
            $('#portrait-chat-toggler').show();
            $('#header_inbox_bar').hide();
            $('#main-menu').removeClass('mini');
            $('.page-content').removeClass('condensed');
        });
        $(window).bind('enterBreakpoint480', function () {
            $('#main-menu-toggle-wrapper').show();
            $('.header-seperation').show();
            $('#portrait-chat-toggler').show();
            $('#header_inbox_bar').hide();
            $('#main-menu').removeClass('mini');
            $('.page-content').removeClass('condensed');
        });
        $(window).bind('enterBreakpoint768', function () {
            $('#main-menu-toggle-wrapper').show();
            $('#portrait-chat-toggler').show();
            $('#header_inbox_bar').hide();
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                $('#main-menu').removeClass('mini');
                $('.page-content').removeClass('condensed');
            }
        });
        $(window).bind('enterBreakpoint1024', function () {
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                var elem = jQuery('.page-sidebar ul');
                elem.children('li.open').children('a').children('.arrow').removeClass('open');
                elem.children('li.open').children('a').children('.arrow').removeClass('active');
                elem.children('li.open').children('.sub-menu').slideUp(200);
                elem.children('li').removeClass('open');
            }
            $('.bar').show();
            $('.bar').css('overflow', 'visible');
        });
        $(window).bind('exitBreakpoint320', function () {
            $('#main-menu-toggle-wrapper').hide();
            $('#portrait-chat-toggler').hide();
            $('#header_inbox_bar').show();
            closeAndRestSider();
        });
        $(window).bind('exitBreakpoint480', function () {
            $('#main-menu-toggle-wrapper').hide();
            $('#portrait-chat-toggler').hide();
            $('#header_inbox_bar').show();
            closeAndRestSider();
        });
        $(window).bind('exitBreakpoint768', function () {
            $('#main-menu-toggle-wrapper').hide();
            $('#portrait-chat-toggler').hide();
            $('#header_inbox_bar').show();
            closeAndRestSider();
        });
        function closeAndRestSider() {
            if ($('#main-menu').attr('data-inner-menu') == '1') {
                $('#main-menu').addClass("mini");
                $('#main-menu').removeClass("left");
            } else {
                $('#main-menu').removeClass("left");
            }
        }

    }

    var handleInitHeader = function() {

        $('#layout-condensed-toggle').on('touchstart click', function (e) {
            e.preventDefault();
        });

        $('#main-menu-toggle').on('touchstart click', function (e) {
            e.preventDefault();
            var timer;
            if ($('body').hasClass('open-menu-left')) {
                $('body').removeClass('open-menu-left');
                timer = setTimeout(function () {
                    $('.page-sidebar').removeClass('visible');
                }, 300);
            }
            else {
                clearTimeout(timer);
                $('.page-sidebar').addClass('visible');
                setTimeout(function () {
                    $('body').addClass('open-menu-left');
                }, 50);
            }
        });

        $('#layout-condensed-toggle').click(function () {
            if ($('#main-menu').attr('data-inner-menu') == '1') {
                console.log("Menu is already condensed");
            } else {
                if ($('#main-menu').hasClass('mini')) {
                    $('body').removeClass('grey');
                    $('body').removeClass('condense-menu');
                    $('#main-menu').removeClass('mini');
                    $('.page-content').removeClass('condensed');
                    $('.scrollup').removeClass('to-edge');
                    $('.header-seperation').show();
                    $('.header-seperation').css('height', '61px');
                    $('.footer-widget').show();
                    $('.page-content').css('margin-left','250px');
                } else {
                    $('body').addClass('grey');
                    $('#main-menu').addClass('mini');
                    $('.page-content').addClass('condensed');
                    $('.scrollup').addClass('to-edge');
                    $('.header-seperation').hide();
                    $('.footer-widget').hide();
                    $('.page-content').css('margin-left','50px');
                    $('.page-content').css('padding-right','-50px');

                    var elem = $('.page-sidebar ul');
                    elem.children('li.open').children('a').children('.arrow').removeClass('open');
                    elem.children('li.open').children('a').children('.arrow').removeClass('active');
                    elem.children('li.open').children('.sub-menu').slideUp(200);
                    elem.children('li').removeClass('open');
                }
            }
        });
    }
    return {
        init : function() {
            handleInit();

            $(window).resize(function() {
                if($(window).width()>=980) {
                    $('.page-sidebar').height($(window).height()-60);
                } else {
                    $('.page-sidebar').height('100%');
                }
            });
        },
        initSidebar : function() {
            handleInitSidebar();
        },
        initSidebarLayout : function() {
            handleInitSidebarLayout();
        },
        initHeader: function() {
            handleInitHeader();
        }
    }
}();
(function ($) {
    $.fn.toggleMenu = function () {
        var windowWidth = window.innerWidth;
        if (windowWidth > 768) {
            $(this).toggleClass('hide-sidebar');
        }
    };
    $.fn.condensMenu = function () {
        var windowWidth = window.innerWidth;
        if (windowWidth > 768) {
            if ($(this).hasClass('hide-sidebar'))$(this).toggleClass('hide-sidebar');
            $(this).toggleClass('condense-menu');
            $(this).find('#main-menu').toggleClass('mini');
        }
    };
})(jQuery);