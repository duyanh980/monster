RocketApp.service('Permission', function ($http, $state) {
    var urlBase = $state.API + 'permissions';
    this.urlBase = urlBase;

    this.find = function (id) {
        return $http.get(urlBase + '/' + id);
    };

    this.insert = function (data) {
        return $http.post(urlBase, data)
    };

    this.update = function (id, data) {
        return $http.put(urlBase  + '/' + id, data)
    };

    this.delete = function (id) {
        return $http.delete(urlBase + '/' + id);
    };

});