RocketApp.service('User', ['$http','$state', function ($http,$state) {
    var urlBase = $state.API + 'users';
    this.urlBase = urlBase;

    this.find = function (id) {
        return $http.get(urlBase + '/' + id);
    };

    this.profile = function (id) {
        if(typeof id === 'undefined') {
            return $http.get(urlBase + '/profile');
        } else {
            return $http.get(urlBase + '/profile/' + id);
        }

    };

    this.insert = function (data) {
        return $http.post(urlBase, data)
    };

    this.update = function (id, data) {
        return $http.put(urlBase  + '/' + id, data)
    };

    this.delete = function (id) {
        return $http.delete(urlBase + '/' + id);
    };

}]);
