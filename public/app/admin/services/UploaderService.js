/**
 * Created by DuyAnk on 2/7/15.
 */
RocketApp.service('Uploader', function ($http, $state) {
    var uploaderUrlBase = $state.API + 'image';
    var folderUrlBase = $state.API + 'directory';

    this.findFolder = function (id) {
        return $http.get(folderUrlBase + '/' + id);
    };

    this.insertFolder = function(data) {
        return $http.post(folderUrlBase, data);
    }

    this.updateFolder = function(id, data) {
        return $http.put(folderUrlBase + '/' + id, data);
    }

    this.deleteDirectory = function(id) {
        return $http.delete(folderUrlBase + '/' + id);
    }

    this.findImages = function(id,data) {
        return $http.get(uploaderUrlBase + '/' + id,data);
    }
});