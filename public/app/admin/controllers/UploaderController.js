/**
 * Created by DuyAnk on 1/28/2015.
 */
RocketApp.controller('UploaderController', function ($scope, $state, $modalInstance, type, Uploader) {
    //set tab default
    $scope.tab = 2;
    $scope.type = type;

    $scope.DirectoryData = {};
    $scope.ImagesData = [];
    $scope.ImageSelected = {};
    $scope.ListImagesSelected = [];
    $scope.isDisable = true;
    $scope.node = {};
    $scope.offset = 0;
    $scope.loading =false;
    //insert or update folder
    $scope.saveDirectory = function () {
        if ($scope.DirectoryData.directory_id > 0) {
            Uploader.updateFolder($scope.DirectoryData.directory_id, $scope.DirectoryData)
                .success(function (response) {
                    if (response.status) {
                        $('#ListDirectories').tree(
                            'renameNode',
                            $scope.node, $scope.DirectoryData.directory_name
                        );
                        $scope.tab = 2;
                    }
                });
        } else {
            $scope.DirectoryData.directory_parent_id = $scope.node.id;
            Uploader.insertFolder($scope.DirectoryData)
                .success(function (response) {
                    if (response.status) {
                        angular.element($('#ListDirectories').tree(
                            'loadDataFromUrl',
                            $state.API + 'directory?node=' + $scope.node.id,
                            $scope.node,
                            function () {
                            }
                        ));
                        $scope.tab = 2;
                    }
                });
        }
    };

    //remove folder
    $scope.deleteDirectory = function (id) {
        Uploader.deleteDirectory(id)
            .success(function (response) {
                if (response.status) {
                    angular.element($('#ListDirectories').tree(
                        'removeNode',
                        $scope.node
                    ));
                }
            });
    }

    //find folder to update
    $scope.findDirectory = function (id) {
        Uploader.findFolder(id)
            .success(function (response) {
                $scope.DirectoryData = response;
                $scope.tab = 3;
            });
    };

    //set button event
    $scope.insert = function () {
        if($scope.type==1) {
            $modalInstance.close($scope.ImageSelected);
        } else {
            $modalInstance.close($scope.ListImagesSelected);
        }
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };


    //list images
    $scope.findImages = function(isNew) {
        if($scope.loading) {
            return;
        }
        $scope.loading = true;
        if($scope.node.id) {
            id = $scope.node.id;
        } else {
            id = 0;
        }
        if(isNew) {
            $scope.offset = 0;
        }
        Uploader.findImages(id,$scope)
            .success(function(response) {
                if(isNew) {
                    angular.element($('#gallery').html(''));
                    $scope.ImagesData = [];
                }
                if(response.length==0) {
                    angular.element($('#gallery').html('<div class="alert alert-info">'+
                        '<button data-dismiss="alert" class="close"></button>'+
                    'Image not found. </div>'));
                } else {
                    $scope.offset += 50;
                }
                $scope.ImagesData = angular.extend($scope.ImagesData, response);
                angular.forEach(response, function(image) {
                    var html = '<li class="attachment" monster-select-item>' +
                        '<div class="attachment-preview ' + image.view_mode + '">'+
                            '<div class="thumbnail-image" data-media-id="' + image.image_id + '">'+
                                '<div class="centered"><img src="' + image.file_full_url + '" /></div>'+
                            '</div>'+
                            '<a class="check" href="javascript:;" title="Deselect">'+
                                '<i class="fa fa-check"></i>'+
                            '</a>'+
                        '</div>'+
                    '</li>';
                    angular.element($('#gallery').append(html));
                });
                $scope.loading = false;
                $scope.eventAfterLoaded();
            });
    }

    //bind event on element
    $scope.eventAfterLoaded = function() {

        $('#gallery .thumbnail-image').unbind('click');
        $('#gallery li .check').unbind('click');
        $('#gallery .check').unbind('mouseover');
        $('#gallery .check').unbind('mouseout');
        angular.element($('.thumbnail-image').bind('click',function() {
            if($(this).parent().parent().hasClass('selected') || $(this).parent().parent().hasClass('selected-before')) {
                $(this).parent().parent().removeClass('selected');
                $(this).parent().parent().removeClass('selected-before');
                $scope.removeImageSelected($(this).data('media-id'));
            } else {
                if($scope.type==2) {
                    $('#gallery li.selected').addClass('selected-before');
                }
                $('#gallery li').removeClass('selected');
                $(this).parent().parent().addClass('selected');
                $scope.selectImage($(this).data('media-id'));
            }
        }));

        angular.element($('#gallery li .check').bind("click",function() {
            $(this).parent().parent().removeClass('selected');
            $(this).parent().parent().removeClass('selected-before');
        }));

        angular.element($('#gallery .check').bind("mouseover",function() {
            $(this).find('.fa').removeClass('fa-check').addClass('fa-minus');
        }));
        angular.element($('#gallery .check').bind("mouseout",function() {
            $(this).find('.fa').removeClass('fa-minus').addClass('fa-check');
        }));
    }

    //select single image
    $scope.selectImage = function(media_id) {
        $scope.ImageSelected = $scope.ImagesData[media_id];
        $scope.ListImagesSelected[media_id] = $scope.ImageSelected;
        //$('.thumbnail-preview img').attr('src',$scope.ImageSelected.file_full_url);
        $scope.ListImagesSelected.clean(undefined);
        $('.addFile').removeAttr('disabled');
    };

    //remove image selected
    $scope.removeImageSelected = function(item) {
        $scope.ImageSelected = {};
        if($scope.type!=1) {
            $.each($scope.ListImagesSelected,function(i) {
                if(this.image_id == item) {
                    delete $scope.ListImagesSelected[i];
                    $scope.ListImagesSelected.clean(undefined);
                }
            });
            if($scope.ListImagesSelected.length ==0) {
                $('.addFile').attr('disabled','disabled');
            }
        } else {
            $('.addFile').attr('disabled','disabled');
        }

    };

    //set modal height
    $scope.setModalHeight = function () {
        var navTabHeight = angular.element($('.nav-tabs').height())[0];
        var modalFooterHeight = angular.element($('.modal-footer').height())[0];
        var windowHeight = angular.element($('.modal-dialog').height())[0];

        angular.element($('.tab-content').height(windowHeight - 60 - navTabHeight - modalFooterHeight));
        $('.mScrollBarFile').height($('.tab-content').height()-40);
        $('.mScrollBar').height($('.tab-content').height()-40);
    }
    $modalInstance.opened.then(function () {
        setTimeout(function () {
            angular.element($scope.setModalHeight());
            angular.element($(".mScrollBarFile").mCustomScrollbar({
                callbacks:{
                    whileScrolling:function(){
                        if(mcs.topPct==100) {
                            $scope.findImages();
                        }
                    }
                }
            }));
            angular.element($(".mScrollBar").mCustomScrollbar());
    }, 300);

    })
    angular.element($(window).resize(function () {
        $scope.setModalHeight();
    }));

    //config uploader
    $scope.uploader = {
        url : $state.API + 'image',
        max_file_size: '2mb',
        drop_element: 'mediaManager'
    }

    //helper
    Array.prototype.clean = function(deleteValue) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] == deleteValue) {
                this.splice(i, 1);
                i--;
            }
        }
        return this;
    };

});

RocketApp.directive('modalWindow', ['$window', '$state', function ($window, $state) {
    return {
        restrict: 'EA',
        link: function (scope, element) {

            angular.element($('#ListDirectories')).tree({
                dataUrl: $state.API + 'directory'
            });
            angular.element($('#ListDirectories').bind(
                'tree.select',
                function (event) {
                    if (event.node) {
                        scope.node = event.node;
                        scope.findImages(true);
                    }
                }
            ));

            angular.element($('#ListDirectories').bind(
                'keith.addFolder',
                function (event) {
                    if (event.node) {
                        scope.node = event.node;
                        scope.DirectoryData = {};
                        scope.tab = 3;
                    }
                }
            ));

            angular.element($('#ListDirectories').bind(
                'keith.removeFolder',
                function (event) {
                    if (event.node) {
                        if (confirm("Are you sure?"))
                            scope.node = event.node;
                        scope.deleteDirectory(event.node.id);
                    }
                }
            ));

            angular.element($('#ListDirectories').bind(
                'keith.editFolder',
                function (event) {
                    if (event.node) {
                        scope.node = event.node;
                        scope.findDirectory(event.node.id);
                    }
                }
            ));
        }
    }
}]);

RocketApp.directive('plupload',function() {
    return {
        link: function (scope, element, attrs) {

            var $container = $('#gallery');
            var uploader = new plupload.Uploader({
                runtimes : 'html5,flash,html4',
                browse_button : element[0],
                drop_element    :   scope.uploader.drop_element,
                url : scope.uploader.url,
                flash_swf_url : 'http://duyanhvn.thejupitech.com/report/public/assets/plugin/plupload/Moxie.swf',
                multipart_params : {directory: 0},
                filters : {
                    max_file_size : scope.uploader.max_file_size,
                    mime_types: [
                        {title : "Image files", extensions : 'jpg,jpeg,png'},
                        /*
                         {title : "Document files", extensions : 'doc, docx, xls, xlsx, pdf, ppt, pptx'},
                         {title : "Media files", extensions : 'mp3, mp4, avi, mkv, wav' },
                         */
                    ]
                },

                init: {
                    FilesAdded: function(up, files) {
                        $('#gallery').find('.alert').remove();
                        uploader.settings.multipart_params.directory = scope.node.id;
                        scope.tab=2;
                        if(scope.type==1)
                            $('.attachment').removeClass('selected');
                        var html = '';
                        for(var i=0;i<files.length;i++) {
                            var cls;
                            if(scope.type==1) {
                                if((files.length-1) == i) {
                                    cls = ' selected';
                                }
                                else {
                                    cls = '';
                                }
                            }
                            else {
                                cls = ' selected';
                            }
                            html = '<li class="attachment' + cls + '">'+
                                '<div class="attachment-preview">'+
                                '<div class="thumbnail-image uploading" data-media-id="" id="'+files[i].id+'">'+
                                '<div class="centered"></div>'+
                                '<div class="progress progress-striped active">'+
                                '<div class="progress-bar progress-bar-primary"></div>'+
                                '</div>'+
                                '</div>'+
                                '<a class="check" href="#" title="Deselect">'+
                                '<i class="fa fa-check"></i>'+
                                '</a>'+
                                '</div>'+
                                '</li>';
                            $container.prepend($(html));
                        }
                        uploader.start();
                    },
                    UploadProgress: function(up, file) {
                        $('#'+file.id).find('.progress-bar').css('width',file.percent+'%');
                    },
                    Error: function(up, err) {
                        $('#message').prepend('<div class="alert alert-error">'+
                            '<button data-dismiss="alert" class="close"></button>' +
                            '<strong>' + err.file.name + ':</strong> '+ err.message +'</div>');
                        scope.tab = 2;
                    },
                    FileUploaded:function(up,files,response)
                    {
                        var res = JSON.parse(response.response);
                        if(res.status) {

                            $('#'+files.id).find('.progress').remove();
                            $('#'+files.id).attr('data-media-id',res.data.image_id);
                            $('#'+files.id).parent('.attachment-preview').addClass(res.data.view_mode);
                            $('#'+files.id).find('.centered').html('<img src="'+res.data.file_full_url+'" />');
                            scope.ImagesData[res.data.image_id] = res.data;
                            scope.selectImage(res.data.image_id);
                            scope.eventAfterLoaded();
                        }
                        else {

                            $('#message').prepend('<div class="alert alert-error">'+
                                '<button data-dismiss="alert" class="close"></button>' +
                                '<strong>'+files.name+': </strong> '+ res.message +'</div>');
                            $('#'+files.id).parent().parent().remove();
                        }
                    }
                }
            });

            uploader.bind('Init', function(up, params) {
                if (uploader.features.dragdrop) {
                    var target = $("#mediaManager");
                    target.on("dragenter",function() {
                        $('#drop-window').show();
                    });

                    $('#drop-window').on("dragleave",function() {
                        $('#drop-window').hide();
                    });

                    $('#drop-window').on("drop",function() {
                        $('#drop-window').hide();
                    });
                }
            });
            uploader.init();

        }
    }
});


