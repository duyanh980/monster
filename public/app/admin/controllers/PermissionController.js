RocketApp.controller('PermissionController', function($scope, $state, $location, $http, $stateParams, $compile, Permission, DTOptionsBuilder, DTColumnBuilder, DTInstances) {
    var Ctrl = this;
    $scope.PermissionData = {};

    $scope.find = function() {
        if($stateParams.permissionID!="") {
            Permission.find($stateParams.permissionID)
                .success(function(response) {
                    $scope.PermissionData = response;
                });
        }
    };

    $scope.save = function() {
        if($stateParams.permissionID!="") {
            Permission.update($stateParams.permissionID,$scope.PermissionData)
                .success(function(response) {
                    if(response.status) {
                        $scope.showMessage('Update successfully','success');
                        $location.path("permissions").replace();
                    }
                });
        } else {
            Permission.insert($scope.PermissionData)
                .success(function(response) {
                    if(response.status) {
                        $scope.showMessage('Update successfully','success');
                        $location.path("permissions").replace();
                    }
                });
        }
    }

    $scope.delete = function(id) {
        Permission.delete(id)
            .success(function(response) {
                Ctrl.dtInstance.reloadData();
            });
    }

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: Permission.urlBase
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('order', [[0, 'desc']])
        .withOption('fnRowCallback',
        function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $compile(nRow)($scope);
        })
        .withOption('fnDrawCallback',
        function () {
            $('.select2-wrapper').select2();
        });


    $scope.dtColumns = [
        DTColumnBuilder.newColumn('permission_id').withTitle('ID'),
        DTColumnBuilder.newColumn('permission_name').withTitle('Permission Name'),
        DTColumnBuilder.newColumn(null).withTitle('Actions').withClass('column-action').notSortable()
            .renderWith($scope.actionsHtml)
    ];
    $scope.actionsHtml = function(data, type, full, meta) {
        return '<a class="label label-success" href="#/permissions/update/' + data.permission_id + '">Edit</a> '+
            '<a class="label label-important" data-ng-click="delete('+ data.permission_id +')" href="javascript:;">Delete</a>';
    }

    DTInstances.getLast().then(function(dtInstance) {
        Ctrl.dtInstance = dtInstance;
    });

});

