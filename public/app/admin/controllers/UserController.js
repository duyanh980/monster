RocketApp.controller('UserController', function($rootScope,$scope, $state, $location, $http, $modal, $stateParams, $compile, User, DTOptionsBuilder, DTColumnBuilder, DTInstances) {
    var Ctrl = this;
    $scope.UserData = {};
    $scope.profile = function() {
        User.find($stateParams.userID)
            .success(function (data) {
                $scope.UserData = data;
            });
    };

    $scope.find = function() {
        if($stateParams.userID!="") {
            User.find($stateParams.userID)
                .success(function (data) {
                    $scope.UserData = data;
                });
        } else {
            User.find(0)
                .success(function (data) {
                    $scope.UserData = data;
                });
        }
    }



    $scope.save = function() {
        if($stateParams.userID!="") {
            User.update($stateParams.userID,$scope.UserData)
                .success(function(response) {
                    if(response.user) {
                        //set user logged in
                        $rootScope.$broadcast('UserInfo', response.user);
                    }
                    if(response.status) {
                        $scope.showMessage('Update successfully','success');
                        $location.path("users").replace();
                    }
                });
        } else {
            User.insert($scope.UserData)
                .success(function(response) {
                    if(response.status) {
                        $scope.showMessage('Update successfully','success');
                        $location.path("users").replace();
                    }
                });
        }
    }

    $scope.delete = function(id) {
        User.delete(id)
            .success(function(response) {
                Ctrl.dtInstance.reloadData();
            });
    }

    //datatable
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: User.urlBase
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('order', [[0, 'desc']])
        .withOption('fnRowCallback',
        function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $compile(nRow)($scope);
        })
        .withOption('fnDrawCallback',
        function () {
            $('.select2-wrapper').select2();
        });


    $scope.dtColumns = [
        DTColumnBuilder.newColumn('user_id').withTitle('ID'),
        DTColumnBuilder.newColumn('first_name').withTitle('First name'),
        DTColumnBuilder.newColumn('last_name').withTitle('Last name'),
        DTColumnBuilder.newColumn('email').withTitle('Email'),
        DTColumnBuilder.newColumn(null).withTitle('Actions').withClass('column-action').notSortable()
            .renderWith($scope.actionsHtml)
    ];
    $scope.actionsHtml = function(data, type, full, meta) {
        return '<a class="label label-success" href="#/users/update/' + data.user_id + '">Edit</a> '+
            '<a class="label label-important" data-ng-click="delete('+ data.user_id +')" href="javascript:;">Delete</a>';
    }

    DTInstances.getLast().then(function(dtInstance) {
        Ctrl.dtInstance = dtInstance;
    });

    //open uploader
    $scope.type = 1; // 1 is single upload, 2 is multiple upload
    $scope.uploader = function () {
        var modalInstance = $modal.open({
            templateUrl: 'views/admin/layout/modal/uploader.html',
            controller: 'UploaderController',
            size: 'lg',
            windowClass: 'uploader-dialog',
            resolve: {
                type: function() {
                    return $scope.type;
                },
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RocketApp',
                        files: [
                            'assets/plugins/jqTree/tree.jquery.js',
                            'assets/plugins/jqTree/jqtree.css',
                            'assets/plugins/plupload/plupload.js',
                            'app/admin/controllers/UploaderController.js',
                            'app/admin/services/UploaderService.js'
                        ]
                    });
                }]
            }
        });

        modalInstance.result.then(function (imageObj) {
            $scope.UserData.avatar = imageObj.file_url;
            $scope.UserData.avatarUrl = imageObj.file_full_url;
            console.log($scope.UserData.avatarUrl);
        }, function () {
        });
    };

});
