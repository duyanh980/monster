RocketApp.controller('GroupController', function($scope, $state, $location, $http, $stateParams, $compile, Group, DTOptionsBuilder, DTColumnBuilder, DTInstances) {
    var Ctrl = this;
    $scope.GroupData = {};

    $scope.find = function() {
        if($stateParams.groupID!="") {
            Group.find($stateParams.groupID)
                .success(function(response) {
                    $scope.GroupData = response;
                    console.log(Object.keys($scope.GroupData).length);
                });
        }
    };
    $scope.save = function() {
        if($stateParams.groupID!="") {
            Group.update($stateParams.groupID,$scope.GroupData)
                .success(function(response) {
                    if(response.status) {
                        $scope.showMessage('Update successfully','success');
                        $location.path("groups").replace();
                    }
                });
        } else {
            Group.insert($scope.GroupData)
                .success(function(response) {
                    if(response.status) {
                        $scope.showMessage('Update successfully','success');
                        $location.path("groups").replace();
                    }
                });
        }
    }
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: Group.urlBase
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('order', [[0, 'desc']])
        .withOption('fnRowCallback',
        function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $compile(nRow)($scope);
        })
        .withOption('fnDrawCallback',
        function () {
            $('.select2-wrapper').select2();
        });


    $scope.dtColumns = [
        DTColumnBuilder.newColumn('group_id').withTitle('ID'),
        DTColumnBuilder.newColumn('group_name').withTitle('Group Name'),
        DTColumnBuilder.newColumn(null).withTitle('Actions').withClass('column-action').notSortable()
            .renderWith($scope.actionsHtml)
    ];
    $scope.actionsHtml = function(data, type, full, meta) {
        return '<a class="label label-success" href="#/groups/update/' + data.group_id + '">Edit</a> '+
            '<a class="label label-important" data-ng-click="delete('+ data.group_id +')" href="javascript:;">Delete</a>';
    }

    DTInstances.getLast().then(function(dtInstance) {
        Ctrl.dtInstance = dtInstance;
    });
});

