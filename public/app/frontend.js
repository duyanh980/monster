/**
 * Created by DuyAnk on 12/30/2014.
 */
var RocketApp = angular.module('RocketApp',[
    "ui.router",
    "ui.bootstrap",
    "oc.lazyLoad"]);

RocketApp.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);

RocketApp.controller('AppController', ['$scope', '$rootScope', function($scope, $rootScope) {
    $scope.$on('$viewContentLoaded', function() {
        Rocket.init(); // init core components
    });
}]);



/* Setup Rounting For All Pages */
RocketApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    // Redirect any unmatched url
    $urlRouterProvider.otherwise('/login');

    $stateProvider
        // User login
        .state('login', {
            url: "/login",
            templateUrl: "views/login.html",
            data: { pageTitle: 'Login' },
            controller: "UserController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RocketApp',
                        files: [
                            'app/controllers/UserController.js',
                            'app/services/UserService.js'
                        ]
                    });
                }]
            }
        })

}]);

/* Init global settings and run the app */
RocketApp.run(["$rootScope", "$state", function($rootScope, $state) {
    $state.BASE_URL = 'http://localhost/monster/public/';
    $state.ADMIN_URL = $state.BASE_URL + 'admin/';
    $state.API = $state.BASE_URL + 'api/';
    $rootScope.$state = $state; // state to be accessed from view
}]);




// Route State Load Spinner(used on page or content load)
RocketApp.directive('ngSpinnerBar', ['$rootScope',
    function($rootScope) {
        return {
            link: function(scope, element, attrs) {
                $(element).delay(350).fadeOut(350);
                $rootScope.$on('$stateChangeStart', function() {
                    $(element).show(); // show loading
                });

                // hide the spinner bar on rounte change success(after the content loaded)
                $rootScope.$on('$stateChangeSuccess', function() {
                    $('body').removeClass('page-on-load'); // remove page loading  - first load
                    $(element).delay(350).fadeOut(350); // hide loading
                });

                // handle errors
                $rootScope.$on('$stateNotFound', function() {
                    $(element).delay(350).fadeOut(350);// hide loading
                });

                // handle errors
                $rootScope.$on('$stateChangeError', function() {
                    $(element).delay(350).fadeOut(350);// hide loading
                });
            }
        };
    }
])

