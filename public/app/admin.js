/**
 * Created by DuyAnk on 12/30/2014.
 */
var RocketApp = angular.module('RocketApp',[
    "ui.router",
    "ui.bootstrap",
    "oc.lazyLoad",
    "datatables"]);


RocketApp.directive('selectRepeat', function() {
    return function(scope, element, attrs) {
        setTimeout(function() {
            $('.select2').select2();
        })
    };
});
RocketApp.controller('AppController', ['$scope', function($scope) {
    $scope.$on('$viewContentLoaded', function() {
        Rocket.init(); // init core components
    });
}]);

RocketApp.controller('HeaderController', ['$rootScope','$scope','$state','$http','User',function($rootScope,$scope, $state, $http, User) {

    $scope.$on('$includeContentLoaded', function() {
        Rocket.initHeader();
    });
    $scope.UserData = {};
    User.profile()
        .success(function(data) {
            //set value for another controller
            $rootScope.$broadcast('UserInfo', data);
        });

    $scope.$on('UserInfo', function (event, arg) {
        $scope.UserData = arg;
    });
}]);

RocketApp.controller('SidebarController', ['$scope',function($scope) {


    //get value from another controller
    $scope.$on('UserInfo', function (event, arg) {
        $scope.UserData = arg;
    });

    $scope.$on('$includeContentLoaded', function() {
        Rocket.initSidebar();
        Rocket.initSidebarLayout();
    });
}]);

/* Setup Rounting For All Pages */
RocketApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    // Redirect any unmatched url
    $urlRouterProvider.otherwise('/users');

    $stateProvider
        // Dashboard
        .state('dashboard', {
            url: "/dashboard",
            templateUrl: "views/admin/layout/dashboard.html",
            data: { pageTitle: 'Admin Dashboard Template' }
        })

        // Users
        .state('users', {
            url: "/users",
            templateUrl: "views/admin/layout/users/list.html",
            data: {pageTitle: 'Users Management'},
            controller: "UserController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RocketApp',
                        files: [
                            'app/admin/controllers/UserController.js',
                            'app/admin/services/UserService.js'
                        ]
                    });
                }]
            }
        })
        .state('usersUpdate', {
            url: "/users/update/{userID}",
            templateUrl: "views/admin/layout/users/update.html",
            data: {pageTitle: 'Users Management'},
            controller: "UserController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RocketApp',
                        files: [
                            'app/admin/controllers/UserController.js',
                            'app/admin/services/UserService.js'
                        ]
                    });
                }]
            }
        })

        // Group
        .state('groups', {
            url: "/groups",
            templateUrl: "views/admin/layout/groups/list.html",
            data: {pageTitle: 'Groups Management'},
            controller: "GroupController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RocketApp',
                        files: [
                            'app/admin/controllers/GroupController.js',
                            'app/admin/services/GroupService.js'
                        ]
                    });
                }]
            }
        })
        .state('groupsUpdate', {
            url: "/groups/update/{groupID}",
            templateUrl: "views/admin/layout/groups/update.html",
            data: {pageTitle: 'Groups Management'},
            controller: "GroupController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RocketApp',
                        files: [
                            'app/admin/controllers/GroupController.js',
                            'app/admin/services/GroupService.js'
                        ]
                    });
                }]
            }
        })

        // Permission
        .state('permissions', {
            url: "/permissions",
            templateUrl: "views/admin/layout/permission/list.html",
            data: {pageTitle: 'Permissions Management'},
            controller: "PermissionController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RocketApp',
                        files: [
                            'app/admin/controllers/PermissionController.js',
                            'app/admin/services/PermissionService.js'
                        ]
                    });
                }]
            }
        })
        .state('permissionUpdate', {
            url: "/permissions/update/{permissionID}",
            templateUrl: "views/admin/layout/permission/update.html",
            data: {pageTitle: 'Permissions Management'},
            controller: "PermissionController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RocketApp',
                        files: [
                            'app/admin/controllers/PermissionController.js',
                            'app/admin/services/PermissionService.js'
                        ]
                    });
                }]
            }
        })

}]);


/* Init global settings and run the app */
RocketApp.run(["$rootScope", "$state", "$http", function($rootScope, $state, $http) {
    $state.BASE_URL = 'http://localhost/monster/public/';
    $state.ADMIN_URL = $state.BASE_URL + 'admin/';
    $state.API = $state.BASE_URL + 'api/';
    $rootScope.$state = $state; // state to be accessed from view

    $rootScope.showMessage = function(msg,type) {
        Messenger().post({message:msg,type:type,showCloseButton:true})
    };
    $http.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';
}]);



// Route State Load Spinner(used on page or content load)
RocketApp.directive('ngSpinnerBar', ['$rootScope',
    function($rootScope) {
        return {
            link: function(scope, element, attrs) {
                $(element).fadeOut(350);
                $rootScope.$on('$stateChangeStart', function() {
                    $(element).show(); // show loading
                });

                // hide the spinner bar on rounte change success(after the content loaded)
                $rootScope.$on('$stateChangeSuccess', function() {
                    $('body').removeClass('page-on-load'); // remove page loading  - first load
                    $(element).fadeOut(350); // hide loading
                });

                // handle errors
                $rootScope.$on('$stateNotFound', function() {
                    $(element).fadeOut(350);// hide loading
                });

                // handle errors
                $rootScope.$on('$stateChangeError', function() {
                    $(element).fadeOut(350);// hide loading
                });
            }
        };
    }
]);


// Handle global LINK click
RocketApp.directive('a', function() {
    return {
        restrict: 'E',
        link: function(scope, elem, attrs) {
            if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
                elem.on('click', function(e) {
                    e.preventDefault(); // prevent link click for above criteria
                });
            }
        }
    };
});


RocketApp.directive('monsterValidate', ['$window', '$parse', function ($window, $parse)
{
    return {
        restrict: 'A',
        require: 'form',
        link: function (scope, element, attributes)
        {

            var fn = $parse(attributes.monsterValidate);
            element.bind('submit', function (event)
            {
                if(!element.validationEngine('validate'))
                {
                    return false;
                }

                scope.$apply(function() {
                    fn(scope, {$event:event});
                });
            });
            angular.element($window).bind('resize', function()
            {
                element.validationEngine('updatePromptsPosition');
            });
        }
    };
}]);
