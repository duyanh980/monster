/**
 * Created by DuyAnk on 12/30/2014.
 */
var LoginApp = angular.module('LoginApp',[
    "ui.router",
    "ui.bootstrap",
    "oc.lazyLoad"]);

/* Setup Rounting For All Pages */
LoginApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    // Redirect any unmatched url
    $urlRouterProvider.otherwise('');

    $stateProvider
        // Dashboard
        .state('login', {
            url: "",
            templateUrl: "views/admin/layout/dashboard.html",
            data: { pageTitle: 'Admin Dashboard Template' }
        });
}]);


/* Init global settings and run the app */
LoginApp.run(["$rootScope", "$state", "$http", function($rootScope, $state, $http) {
    $state.BASE_URL = 'http://localhost/monster/public/';
    $state.ADMIN_URL = $state.BASE_URL + 'admin/';
    $state.API = $state.BASE_URL + 'api/';
    $rootScope.$state = $state; // state to be accessed from view

    $http.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';
}]);



// Route State Load Spinner(used on page or content load)
LoginApp.directive('ngSpinnerBar', ['$rootScope',
    function($rootScope) {
        return {
            link: function(scope, element, attrs) {
                $(element).delay(350).fadeOut(350);
                $rootScope.$on('$stateChangeStart', function() {
                    $(element).show(); // show loading
                });

                // hide the spinner bar on rounte change success(after the content loaded)
                $rootScope.$on('$stateChangeSuccess', function() {
                    $('body').removeClass('page-on-load'); // remove page loading  - first load
                    $(element).delay(350).fadeOut(350); // hide loading
                });

                // handle errors
                $rootScope.$on('$stateNotFound', function() {
                    $(element).delay(350).fadeOut(350);// hide loading
                });

                // handle errors
                $rootScope.$on('$stateChangeError', function() {
                    $(element).delay(350).fadeOut(350);// hide loading
                });
            }
        };
    }
]);


// Handle global LINK click
LoginApp.directive('a', function() {
    return {
        restrict: 'E',
        link: function(scope, elem, attrs) {
            if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
                elem.on('click', function(e) {
                    e.preventDefault(); // prevent link click for above criteria
                });
            }
        }
    };
});


LoginApp.controller('UserController', function($scope, $state, User) {
    $scope.UserData = {};
    $scope.ErrorMessage = false;
    $scope.login = function() {
        User.login($scope.UserData)
            .success(function(data) {
                if(data.status) {
                    top.window.location.href = data.url;
                } else {
                    $scope.ErrorMessage = true;
                }
            });
    };
});

LoginApp.service('User', ['$http','$state', function ($http,$state) {
    var urlBase = $state.API + 'users';
    this.urlBase = urlBase;

    this.login = function (data) {
        return $http.post(urlBase + '/login',data);
    };
}]);
