<?php
/**
 * Product: Monster
 * Author: duyanh980@gmail.com
 */
use \Illuminate\Support\Facades\Input;
class RESTController extends Controller {


    public function __construct() {

    }

    public function getConfigs() {
        if(Input::get('draw') && Input::get('columns')) {

            $configTable = new \stdClass();
            $columns = Input::get('columns');
            $order = Input::get('order');
            $search = Input::get('search');
            if(!empty($columns)) {
                foreach($columns as $k=> $column) {
                    //set config columns
                    if($column['data']!="") {
                        $configTable->columns[$k] = new \stdClass();
                        $configTable->columns[$k]->name = $column['data'];
                        $configTable->columns[$k]->orderable = $column['orderable'];
                        $configTable->columns[$k]->searchable = $column['searchable'];
                        $configTable->columns[$k]->searchvalue = $column['search']['value'];
                        $configTable->select[] = $configTable->columns[$k]->name;

                    }
                }
            }

            //set config order
            $configTable->orderBy = $configTable->columns[$order[0]['column']];
            $configTable->orderDir = $order[0]['dir'];
            $configTable->searchvalue = $search['value'];
            $configTable->take = (int)Input::get('length');
            $configTable->skip = (int)Input::get('start');

            return $configTable;
        }
    }
}