<?php
/**
 * Product: Monster
 * Author: duyanh980@gmail.com
 */

class BackendController extends BaseController {

	public function __construct() {
        parent::__construct();
    }

    public function getIndex() {
        if(!\App\Modules\Users\Libraries\Auth::check()) {
            return Redirect::route('homepage');
        }
        return View::make("admin");
    }

}
