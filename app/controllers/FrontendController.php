<?php
/**
 * Product: Monster
 * Author: duyanh980@gmail.com
 */
use \View;
class FrontendController extends BaseController {

    public function __construct() {
        parent::__construct();
    }

    public function getLogin() {
        return View::make('login');
    }

    public function getLogout() {
        \App\Modules\Users\Libraries\Auth::logout();
        return \Illuminate\Support\Facades\Redirect::route('homepage');
    }
}