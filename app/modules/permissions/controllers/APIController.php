<?php namespace App\Modules\Permissions\Controllers;
/**
 * Product: Monster
 * Author: duyanh980@gmail.com
 */
use  App\Modules\Permissions\Models\Permission;
use App\Modules\Users\Libraries\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;


class APIController extends \RESTController {

    public function __construct() {
        parent::__construct();
        if(!Auth::HasPermission("Permission.Permissions.Manager")) {
            return Response::json(array("status"=>false,"message"=>Lang::get('You dont have permission to view this page')));
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function actionIndex() {
        $configTable = $this->getConfigs();

        //get total
        $query = Permission::query();
        if(!empty($configTable->columns)) {
            foreach($configTable->columns as $k => $column) {
                if($column->searchable) {
                    $query->orWhere($column->name,"LIKE","{$configTable->searchvalue}%");
                }
            }
        }
        $totalFiltered = $query->count();
        $totalRecords = Permission::count();

        //select query
        $query = Permission::query();
        $query->select($configTable->select);

        if(!empty($configTable->columns)) {
            foreach($configTable->columns as $k => $column) {
                if($column->searchable) {
                    $query->orWhere($column->name,"LIKE","{$configTable->searchvalue}%");
                }
            }
        }

        $data = $query->orderBy($configTable->orderBy->name,$configTable->orderDir)->skip($configTable->skip)->take($configTable->take)->get();

        $response = [
            "draw"              =>  Input::get('draw'),
            "recordsTotal"      =>  $totalRecords,
            "recordsFiltered"   =>  $totalFiltered,
            "data"              =>  $data
        ];
        return Response::json($response);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function actionView($id) {

        return Response::json(Permission::find($id));
    }

    public function actionCreate() {
        return Response::json(array("status" => $this->postUpdate()));
    }

    public function actionUpdate($id) {
        return Response::json(array("status" => $this->postUpdate($id)));
    }

    public function actionDelete($id) {
        $result = Permission::where("permission_id",$id)->delete();
        return Response::json(array('status'=>$result));
    }

    private function postUpdate($id=false) {
        if($this->checkValidator($id)) {
            $permission = Permission::firstOrNew(array('permission_id'=>$id));
            $permission->fill(Input::all());
            $permission->user_id = Auth::getId();
            return $permission->save();
        }
        return false;
    }

    private function checkValidator($id=false) {
        $rules = Permission::$rules;
        if($id) {
            $rules['permission_name'] = 'required|max:50|unique:Permissions,permission_name,' .$id. ',permission_id';
        }
        $validator = Validator::make(Input::all(),$rules);
        if ($validator->passes()) {
            return true;
        }
        $messages = $validator->messages();
        return false;
    }
}