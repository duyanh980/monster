<?php
/**
 * Product: Monster
 * Author: duyanh980@gmail.com
 */

Route::group(array('prefix'=>'api', 'namespace'=>'App\\Modules\\Permissions\\Controllers'),function() {
    Route::pattern('id', '[0-9]+');
    Route::get('permissions',           array('uses'  =>  'APIController@actionIndex'));
    Route::get('permissions/{id}',      array('uses'  =>  'APIController@actionView'));
    Route::post('permissions',          array('uses'  =>  'APIController@actionCreate'));
    Route::put('permissions/{id}',      array('uses'  =>  'APIController@actionUpdate'));
    Route::delete('permissions/{id}',      array('uses'  =>  'APIController@actionDelete'));

});