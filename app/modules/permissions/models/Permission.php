<?php namespace App\Modules\Permissions\Models;
/**
 * Product: Monster
 * Author: duyanh980@gmail.com
 */

class Permission extends \Eloquent {

    protected $table = 'permissions';
    protected $primaryKey = 'permission_id';
    protected $fillable = array('permission_name','permission_description');

    public static $rules = array(
        'permission_name'=>'required|min:2|max:50|unique:permissions',
        'permission_description'    =>  'required|min:10',
    );

}