<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Images extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('directories', function(Blueprint $table)
		{
            $table->increments('directory_id');
            $table->string('directory_name');
            $table->integer('directory_parent_id')->default(0);
            $table->integer('user_id');
            $table->timestamps();

            Schema::create('images',function(Blueprint $table) {
                $table->increments('image_id');
                $table->string('file_name');
                $table->string('file_url');
                $table->string('file_description');
                $table->string('file_type');
                $table->integer('file_size');
                $table->integer('file_width');
                $table->integer('file_height');
                $table->string('view_mode');
                $table->integer('directory_id')->default(0);
                $table->integer('user_id');
                $table->timestamps();
            });
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('images');
        Schema::drop('directories');
	}

}
