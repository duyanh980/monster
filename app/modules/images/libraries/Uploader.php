<?php namespace App\Modules\Images\Libraries;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;

/**
 * Product: Monster
 * Author: duyanh980@gmail.com
 */

class Uploader {

    protected static $error = "";
    protected static $fileInfo = array();

    public static function upload($config=array(),$inputName='') {
        $defaultConfig = array(
            'path'  =>  '',
            'size'  =>  '200000000',
            'type'  =>  'jpg|jpeg|png'
        );
        $config = array_merge($defaultConfig,$config);
        $config['path'] = 'uploads/'.$config['path'];
        if(!$config['path'] = self::_checkDirectory($config['path'])) {
            return false;
        }
        if (Input::hasFile($inputName)) {
            $file = Input::file($inputName);
            if(self::_checkFileType($file,$config['type']) AND self::_checkFileSize($file,$config['size'])) {
                $path = public_path($config['path']);
                $filename = self::_getFileName($file->getClientOriginalName());
                $newFileName = self::getFileName($path,$filename,$file->getClientOriginalExtension());
                if($file->move($path,$newFileName.'.'.$file->getClientOriginalExtension())) {
                    self::$fileInfo = array(
                        'path'      =>  $config['path'],
                        'file_name'        =>  $newFileName,
                        'file_name'   =>  $newFileName.'.'.$file->getClientOriginalExtension(),
                        'file_url'   =>  $config['path'].$newFileName.'.'.$file->getClientOriginalExtension(),
                        'file_size'   =>  $file->getClientSize(),
                    );
                    if(in_array($file->getClientOriginalExtension(),array("jpg","jpeg","png","gif","bmp"))) {
                        list($width, $height) = getimagesize(public_path(self::$fileInfo['file_url']));
                        self::$fileInfo['file_width'] = $width;
                        self::$fileInfo['file_height'] = $height;
                        if($width>$height) {
                            self::$fileInfo['view_mode'] = 'landscape';
                        }
                        else {
                            self::$fileInfo['view_mode'] = 'portrait';
                        }
                    }
                    return true;
                }
                else {
                    self::$error = Lang::get('images::upload.upload_not_writable');
                    return false;
                }
            }
        }
        else {
            self::$error = Lang::get('images::upload.upload_no_file_selected');
            return false;
        }
    }

    private static function _checkDirectory($directory) {
        if(is_dir($directory)) {
            $Year = date("Y").'/';
            $Month = date("M").'/';

            $folder = $directory.$Year.$Month;
            if(!is_dir($directory = str_replace("/","\\",$directory.$Year))) {
                mkdir($directory,0777);
                if(!is_dir($directory = str_replace("/","\\",$directory.$Month)))
                    mkdir($directory,0777);
            }
            return $folder;
        }
        self::$error = Lang::get('images::upload.upload_no_filepath');
        return false;
    }

    private static function _checkFileSize($file,$sizeAllowed) {
        $size = $file->getSize();
        if($size>$sizeAllowed) {
            self::$error = Lang::get('images::upload.upload_invalid_filesize');
            return false;
        }
        return true;
    }

    private static function _checkFileType($file,$type='') {
        $fileExtendsion = $file->getClientOriginalExtension();
        $fileExtAllowed = explode("|",$type);
        if(empty($fileExtendsion) OR !in_array($fileExtendsion,$fileExtAllowed)) {
            self::$error = Lang::get('images::upload.upload_invalid_filetype');
            return false;
        }

        $mineType = Config::get('mines.'.$fileExtendsion);
        if(!is_array($mineType)) {
            $mineType = array($mineType);
        }
        if(!empty($mineType) AND in_array($file->getMimeType(),$mineType)) {
            return true;
        }
        else {
            self::$error = Lang::get('images::upload.upload_invalid_filetype');
            return false;
        }
    }

    private static function getFileName($path,$fileName,$fileExtendsion,$i=1) {
        if($i==1) {
            if(is_file($path.'/'.$fileName.'.'.$fileExtendsion)) {
                ++$i;
                return self::getFileName($path,$fileName,$fileExtendsion,$i);
            }
            return $fileName;
        }
        else {
            if(is_file($path.'/'.$fileName.'_'.$i.'.'.$fileExtendsion)) {
                ++$i;
                return self::getFileName($path,$fileName,$fileExtendsion,$i);
            }
            return $fileName.'_'.$i;
        }
    }
    public static function fileInfo() {
        return self::$fileInfo;
    }


    public static function error() {
        return self::$error;
    }

    private static function _getFileName($filename) {
        return preg_replace( '/\.[a-z0-9]+$/i' , '' , $filename);
    }
}