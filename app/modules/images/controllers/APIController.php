<?php namespace App\Modules\Images\Controllers;
use App\Modules\Images\Libraries\Uploader;
use App\Modules\Images\Models\Directory;
use App\Modules\Images\Models\Image;
use App\Modules\Users\Libraries\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Response;

/**
 * Product: Monster
 * Author: duyanh980@gmail.com
 */

class APIController extends \RESTController {

    public function __construct() {
        parent::__construct();
        if(!Auth::HasPermission("Permission.Uploader.Manager")) {
            return Response::json(array("status"=>false,"message"=>Lang::get('You dont have permission to view this page')));
        }
    }

    public function actionListDirectories() {
        $id =0;
        if(Input::get('node')>0) {
            $id=Input::get('node');
        }
        $directories = Directory::where('directory_parent_id',$id)->where('user_id',Auth::user()->user_id)->get();
        $data = array();
        if(!empty($directories)) {
            $key=0;
            foreach($directories as $directory) {
                $data[$key] = new \stdClass();
                $data[$key]->id = $directory->directory_id;
                $data[$key]->label = $directory->directory_name;
                $data[$key]->children = array();
                $hasChildren = Directory::where('directory_parent_id',$directory->directory_id)->get();
                if(!$hasChildren->isEmpty()) {
                    $data[$key]->load_on_demand = true;
                    $data[$key]->children = $hasChildren;
                }
                else {
                    $data[$key]->load_on_demand = false;
                }
                ++$key;
            }
        }
        if($id==0 AND Input::get('node')!=='0') {
            $listDirectory[0] =  new \stdClass();
            $listDirectory[0]->id = 0;
            $listDirectory[0]->label = 'Root';
            $listDirectory[0]->children = $data;
            $listDirectory[0]->load_on_demand = false;
            $listDirectory[0]->isRoot = true;
        }
        else {
            $listDirectory = $data;
        }
        return Response::json($listDirectory);
    }

    public function actionViewDirectory($id) {
        return Response::json(Directory::find($id));
    }

    public function actionCreateDirectory() {
        return Response::json(array('status'=>$this->postUpdate()));
    }

    public function actionUpdateDirectory($id) {
        return Response::json(array('status'=>$this->postUpdate($id)));
    }

    public function actionDeleteDirectory($id) {
        return Response::json(array('status'=>Directory::where('directory_id',$id)->delete()));
    }

    private function postUpdate($id=false) {
        if($this->checkValidator($id)) {
            $directory = Directory::firstOrNew(array('directory_id'=>$id));
            $directory->fill(Input::all());
            $directory->user_id = Auth::getId();
            return $directory->save();
        }
        return false;
    }

    private function checkValidator($id=false) {
        return true;
    }

    public function actionIndex($directory_id=0) {
        $getMedia = Image::query();
        if($directory_id>0) {
            $getMedia->where('directory_id',$directory_id);
        }
        $media = $getMedia->where('user_id',Auth::user()->user_id)->skip(Input::get('offset'))->take(50)->orderBy('updated_at')->get();
        $data = array();
        if(!$media->isEmpty()) {
            foreach($media as $var) {
                $k = $var->image_id;
                $data[$k] = new \stdClass();
                $data[$k]->image_id         =   $var->image_id;
                $data[$k]->file_full_url   =   url($var->file_url);
                $data[$k]->file_url   =   $var->file_url;
                $data[$k]->file_name  =   $var->file_name;
                $data[$k]->file_size  =   $var->file_size;
                $data[$k]->file_width =   $var->file_width;
                $data[$k]->file_height=   $var->file_height;
                $data[$k]->updated_at       =   $var->updated_at->diffForHumans();
                if($var->file_width>$var->file_height) {
                    $data[$k]->view_mode = 'landscape';
                }
                else {
                    $data[$k]->view_mode = 'portrait';
                }
            }
        }
        return Response::json($data);
    }

    public function actionCreate() {
        $directory = Directory::where('directory_id',Input::get('directory'))->pluck('directory_id');
        if(!$directory) {
            $directory = 0;
        }
        $config = array(
            'path'  =>  '',
            'type'  =>  'jpg|png|jpeg|bmp',
            'size'  =>  '200000000'
        );
        if(Uploader::upload($config,'file')) {
            $data = Uploader::fileInfo();
            $image = new Image();
            $image->fill($data);
            $image->directory_id = $directory;
            $image->user_id = Auth::user()->user_id;
            $image->save();
            $data['image_id'] = $image->image_id;
            $data['file_full_url'] = url($image->file_url);
            $data['updated_at'] =   $image->updated_at->diffForHumans();

            $response = array(
                'status'    =>  true,
                'data'      =>  $data
            );
        }
        else {
            $response = array(
                'status'    =>  false,
                'message'   =>  Uploader::error()
            );
        }
        return Response::json($response);
    }
}