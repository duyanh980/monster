<?php
/**
 * Product: Monster
 * Author: duyanh980@gmail.com
 */

Route::group(array('prefix'=>'api', 'namespace'=>'App\\Modules\\Images\\Controllers'),function() {
    Route::get('directory',           array('uses'  =>  'APIController@actionListDirectories'));
    Route::get('directory/{id}',           array('uses'  =>  'APIController@actionViewDirectory'));
    Route::post('directory',           array('uses'  =>  'APIController@actionCreateDirectory'));
    Route::put('directory/{id}',           array('uses'  =>  'APIController@actionUpdateDirectory'));
    Route::delete('directory/{id}',           array('uses'  =>  'APIController@actionDeleteDirectory'));


    Route::post('image',           array('uses'  =>  'APIController@actionCreate'));
    Route::get('image/{id}',           array('uses'  =>  'APIController@actionIndex'));

});