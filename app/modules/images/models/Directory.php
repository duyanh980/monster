<?php namespace App\Modules\Images\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Product: Monster
 * Author: duyanh980@gmail.com
 */

class Directory extends Model {

    protected $table = 'directories';
    protected $primaryKey = 'directory_id';

    protected $fillable = array('directory_name','directory_parent_id');

    public static $rules = array(
        'directory_name'=>'required|min:2|max:50|unique:groups',
    );
}