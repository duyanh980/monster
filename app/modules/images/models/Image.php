<?php namespace App\Modules\Images\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Product: Monster
 * Author: duyanh980@gmail.com
 */

class Image extends Model {

    protected $table = 'images';
    protected $primaryKey = 'image_id';

    protected $fillable = array('file_name', 'file_url', 'file_description', 'file_type', 'file_size', 'file_width', 'file_height', 'view_mode', 'directory_id');

    public static $rules = array(
        'file_name' =>  'required',
        'file_url'  =>  'required',
    );
}