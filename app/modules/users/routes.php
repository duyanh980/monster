<?php
/**
 * Product: Monster
 * Author: duyanh980@gmail.com
 */

Route::group(array('prefix'=>'api', 'namespace'=>'App\\Modules\\Users\\Controllers'),function() {
    Route::pattern('id', '[0-9]+');
    Route::get('users',           array('uses'  =>  'APIController@actionIndex'));
    Route::get('users/{id}',      array('uses'  =>  'APIController@actionView'));
    Route::get('users/profile/{id?}',      array('uses'  =>  'APIController@actionProfile'));
    Route::post('users',          array('uses'  =>  'APIController@actionCreate'));
    Route::put('users/{id}',      array('uses'  =>  'APIController@actionUpdate'));
    Route::post('users/login',      array('uses'  =>  'APIController@actionLogin'));

});