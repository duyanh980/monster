<?php namespace App\Modules\Users\Seeds;
use App\Modules\Users\Models\User;
use Illuminate\Support\Facades\Hash;
/**
 * Product: Monster
 * Author: duyanh980@gmail.com
 */


class DatabaseSeeder extends \Seeder {

    public function run() {
        User::truncate();
        User::create(array(
            'first_name'    =>  'The',
            'last_name'     =>  'Rocket',
            'email'         =>  'monster@gmail.com',
            'password'      =>  Hash::make('monster@hda'),
            'status'        =>  '1',
            'group_id'       =>  '1',
        ));

    }
}