<?php namespace App\Modules\Users\Models;

use App\Modules\Group\Models\Group;
use App\Modules\User\Libraries\Auth, Config;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Support\Facades\HTML;


class User extends \Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
    protected $primaryKey = 'user_id';

    protected $guarded = array('password');
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
    protected $fillable = array('first_name', 'last_name', 'email', 'password', 'avatar','group_id','status');

    public static $rules = array(
        'first_name'=>'required|alpha|min:2|max:50',
        'last_name'=>'required|alpha|min:2|max:50',
        'email'    => 'required|email|unique:users',
        'password' => 'required',
    );

    protected $dates =  array('last_login');
	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}
    public function getRememberToken()
    {
        return $this->token;
    }

    public function setRememberToken($value)
    {
        $this->token = $value;
    }

    public function getRememberTokenName()
    {
        return 'token';
    }

}