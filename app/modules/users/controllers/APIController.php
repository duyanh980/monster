<?php namespace App\Modules\Users\Controllers;
use App\Modules\Groups\Models\Group;
use App\Modules\Users\Libraries\Auth;
use App\Modules\Users\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Response;

/**
 * Product: Monster
 * Author: duyanh980@gmail.com
 */

class APIController extends \RESTController {

    public function __construct() {
        parent::__construct();
    }


    public function actionLogin() {
        if(Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password'), 'status'=>1),Input::get('remember'))) {
            $user = User::find(Auth::user()->user_id);
            $user->last_login = Carbon::now();
            $user->save();
            if(Auth::HasPermission('Website.Backend.Login')) {
                $response = array("status"=>true,"url"=>route("admin.dashboard"));
            }
            else {
                $response = array("status"=>true,"url"=>route("admin.dashboard"));
            }
        } else {
            $response = array("status"=>false,"message"=>"Invalid email or password.");
        }
        return Response::json($response);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function actionIndex() {
        if(!Auth::HasPermission("Permission.Users.Manager")) {
            return Response::json(array("status"=>false,"message"=>Lang::get('You dont have permission to view this page')));
        }
        $configTable = $this->getConfigs();

        //get total
        $query = User::query();
        if(!empty($configTable->columns)) {
            foreach($configTable->columns as $k => $column) {
                if($column->searchable) {
                    $query->orWhere($column->name,"LIKE","{$configTable->searchvalue}%");
                }
            }
        }
        $totalFiltered = $query->count();
        $totalRecords = User::count();

        //select query
        $query = User::query();
        $query->select($configTable->select);

        if(!empty($configTable->columns)) {
            foreach($configTable->columns as $k => $column) {
                if($column->searchable) {
                    $query->orWhere($column->name,"LIKE","{$configTable->searchvalue}%");
                }
            }
        }
        $data = $query->orderBy($configTable->orderBy->name,$configTable->orderDir)->skip($configTable->skip)->take($configTable->take)->get();

        $response = [
            "draw"              =>  Input::get('draw'),
            "recordsTotal"      =>  $totalRecords,
            "recordsFiltered"   =>  $totalFiltered,
            "data"              =>  $data
        ];
        return Response::json($response);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function actionView($id) {
        if(!Auth::HasPermission("Permission.Users.Manager") && $id!=Auth::getId()) {
            return Response::json(array("status"=>false,"message"=>Lang::get('You dont have permission to view this page')));
        }
        $user = User::find($id);
        if($user == "") {
            $user = new \stdClass();
        } else {
            unset($user->password);
        }
        $groups = Group::all();
        $data = array();
        if(!$groups->isEmpty()) {
            foreach($groups as $group) {
                if(Auth::HasPermission('Group.'.$group->group_name.'.Manager'))
                    $data[] = $group;
            }
        }
        $user->avatarUrl = url($user->avatar);
        $user->groups = $data;
        return Response::json($user);
    }

    public function actionProfile($id = false) {
        if(!$id) {
            $id = Auth::getId();
        }
        $user = User::find($id);
        $user->avatarUrl = url($user->avatar);
        unset($user->password);
        return Response::json($user);
    }

    public function actionCreate() {
        if(!Auth::HasPermission("Permission.Users.Manager")) {
            return Response::json(array("status"=>false,"message"=>Lang::get('You dont have permission to do this action')));
        }
        return Response::json(array("status" => $this->postUpdate()));
    }

    public function actionUpdate($id) {
        if(!Auth::HasPermission("Permission.Users.Manager") && $id!=Auth::getId()) {
            return Response::json(array("status"=>false,"message"=>Lang::get('You dont have permission to do this action')));
        }
        $response = array("status" => $this->postUpdate($id));
        if(Auth::getId()==$id) {
            $response['user'] = User::find($id);
            $response['user']->avatarUrl = url($response['user']->avatar);
        }
        return Response::json($response);
    }

    private function postUpdate($id=false) {
        if($this->checkValidator($id)) {
            $user = User::firstOrNew(array('user_id'=>$id));
            $user->fill(Input::except('group_id','password'));
            if(Input::get('password')!="") {
                $user->password = Hash::make(Input::get('password'));
            }
            $group = Group::where('group_id',Input::get('group_id'))->first();
            if(Auth::HasPermission('Group.'.$group->group_name.'.Manager')) {
                $user->group_id = $group->group_id;
            }
            return $user->save();
        }
        return false;
    }

    private function checkValidator($id=false) {
        return true;
    }
}