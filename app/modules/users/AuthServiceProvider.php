<?php namespace App\Modules\Users;

use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider {

	public function register()
	{
		\Log::debug("AuthServiceProvider registered");
	}

}
