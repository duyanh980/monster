<?php namespace App\Modules\Users\Libraries;

use App\Modules\Permissions\Models\Permission;

class Auth extends \Illuminate\Support\Facades\Auth {


    public static function getId() {
        return self::user()->user_id;
    }

    public static function HasPermission($permission) {
        if(!Auth::check()) {
            return false;
        }
        $groupID = Auth::user()->group_id;
        $check = Permission::join('group_permission','group_permission.permission_id','=','permissions.permission_id')->where('group_id',$groupID)->where('permission_name',$permission)->first();

        if($check) {
            return true;
        }
        return false;
    }
}