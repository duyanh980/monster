<?php namespace App\Modules\Groups\Controllers;
use App\Modules\Groups\Models\Group;
use App\Modules\Users\Libraries\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Response;

/**
 * Product: Monster
 * Author: duyanh980@gmail.com
 */

class APIController extends \RESTController {

    public function __construct() {
        parent::__construct();
        if(!Auth::HasPermission("Permission.Groups.Manager")) {
            return Response::json(array("status"=>false,"message"=>Lang::get('You dont have permission to view this page')));
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function actionIndex() {
        $configTable = $this->getConfigs();

        //get total
        $query = Group::query();
        if(!empty($configTable->columns)) {
            foreach($configTable->columns as $k => $column) {
                if($column->searchable) {
                    $query->orWhere($column->name,"LIKE","{$configTable->searchvalue}%");
                }
            }
        }
        $totalFiltered = $query->count();
        $totalRecords = Group::count();

        //select query
        $query = Group::query();
        $query->select($configTable->select);

        if(!empty($configTable->columns)) {
            foreach($configTable->columns as $k => $column) {
                if($column->searchable) {
                    $query->orWhere($column->name,"LIKE","{$configTable->searchvalue}%");
                }
            }
        }
        $data = $query->orderBy($configTable->orderBy->name,$configTable->orderDir)->skip($configTable->skip)->take($configTable->take)->get();

        $response = [
            "draw"              =>  Input::get('draw'),
            "recordsTotal"      =>  $totalRecords,
            "recordsFiltered"   =>  $totalFiltered,
            "data"              =>  $data
        ];
        return Response::json($response);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function actionView($id) {
        $group = Group::find($id);
        if($group=="") {
            $group = new \stdClass();
        }
        Group::get_group_permissions($group);

        $permissions_full = $group->permissions;
        $group_permissions = $group->group_permissions;
        $template = array();

        if(!empty($permissions_full)) {
            foreach ($permissions_full as $perm) {
                $template[$perm->permission_name]['perm_id'] = $perm->permission_id;
                $template[$perm->permission_name]['value'] = 0;
                if(isset($group_permissions[$perm->permission_id]) )
                {
                    $template[$perm->permission_name]['value'] = 1;
                }
            }
        }

        $domains = array();
        if(!empty($template)) {
            foreach ($template as $key => $value) {
                list($domain, $name, $action) = @explode('.', $key);
                if($domain!="" && $name!="" && $action!="") {
                    if (!empty($domain) && !array_key_exists($domain, $domains)) {
                        $domains[$domain] = array();
                    }
                    if (!isset($domains[$domain][$name])) {
                        $domains[$domain][$name] = array(
                            $action => $value
                        );
                    }
                    else {
                        $domains[$domain][$name][$action] = $value;
                    }

                    // Store the actions separately for building the table header
                    if (!isset($domains[$domain]['actions'])) {
                        $domains[$domain]['actions'] = array();
                    }

                    if (!in_array($action, $domains[$domain]['actions'])) {
                        $domains[$domain]['actions'][] = $action;
                    }
                }

            }//end foreach
        }
        $group->domains = $domains;
        return Response::json($group);
    }

    public function actionCreate() {
        return Response::json(array("status" => $this->postUpdate()));
    }

    public function actionUpdate($id) {
        return Response::json(array("status" => $this->postUpdate($id)));
    }

    public function actionDelete($id) {
        $result = Group::where("group_d",$id)->delete();
        return Response::json(array('status'=>$result));
    }

    /**
     * @param int $id
     */
    private function postUpdate($id=false) {
        if($this->checkValidator($id)) {
            $group = Group::firstOrNew(array('group_id'=>$id));
            $group->fill(Input::all());
            $group->save();

            if($group->exists) {
                $group->set_group_permission($id,Input::get('group_permissions'));
            }
            return $group->save();
        }
        return false;
    }

    /**
     * check validation value
     * @param int $id
     * @return bool
     */
    private function checkValidator($id=false) {
        return true;
    }
}