<?php namespace App\Modules\Groups\Seeds;
use App\Modules\Groups\Models\Group;
use App\Modules\Permissions\Models\Permission;
use Illuminate\Support\Facades\DB;

/**
 * Product: Monster
 * Author: duyanh980@gmail.com
 */


class DatabaseSeeder extends \Seeder {

    public function run() {
        Group::truncate();
        Permission::truncate();
        DB::table('group_permission')->truncate();
        $administrator = new Group();
        $administrator->group_name = 'Administrator';
        $administrator->save();

        $user = new Group();
        $user->group_name = 'User';
        $user->save();

        $websiteFrontendLogin = new Permission();
        $websiteFrontendLogin->permission_name = "Website.Frontend.Login";
        $websiteFrontendLogin->save();

        $websiteBackendLogin = new Permission();
        $websiteBackendLogin->permission_name = "Website.Backend.Login";
        $websiteBackendLogin->save();

        $permissionUsersManager = new Permission();
        $permissionUsersManager->permission_name = "Permission.Users.Manager";
        $permissionUsersManager->save();

        $permissionGroupsManager = new Permission();
        $permissionGroupsManager->permission_name = "Permission.Groups.Manager";
        $permissionGroupsManager->save();

        $permissionPermissionsManager = new Permission();
        $permissionPermissionsManager->permission_name = "Permission.Permissions.Manager";
        $permissionPermissionsManager->save();

        $permissionUploaderManager = new Permission();
        $permissionUploaderManager->permission_name = "Permission.Uploader.Manager";
        $permissionUploaderManager->save();


        $groupAdministratorManager = new Permission();
        $groupAdministratorManager->permission_name = "Group.Administrator.Manager";
        $groupAdministratorManager->save();
        $groupUsersManager = new Permission();
        $groupUsersManager->permission_name = "Group.User.Manager";
        $groupUsersManager->save();

        $group_permission = [
            [
                "group_id"  =>  $administrator->group_id,
                "permission_id" =>  $groupAdministratorManager->permission_id
            ],[
                "group_id"  =>  $administrator->group_id,
                "permission_id" =>  $websiteFrontendLogin->permission_id
            ],[
                "group_id"  =>  $administrator->group_id,
                "permission_id" =>  $websiteBackendLogin->permission_id
            ],[
                "group_id"  =>  $administrator->group_id,
                "permission_id" =>  $permissionUsersManager->permission_id
            ],[
                "group_id"  =>  $administrator->group_id,
                "permission_id" =>  $permissionGroupsManager->permission_id
            ],[
                "group_id"  =>  $administrator->group_id,
                "permission_id" =>  $permissionPermissionsManager->permission_id
            ],[
                "group_id"  =>  $administrator->group_id,
                "permission_id" =>  $permissionUploaderManager->permission_id
            ],[
                "group_id"  =>  $user->group_id,
                "permission_id" =>  $websiteFrontendLogin->permission_id
            ],
        ];
        DB::table('group_permission')->insert($group_permission);
    }
}