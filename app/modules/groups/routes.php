<?php
/**
 * Product: Monster
 * Author: duyanh980@gmail.com
 */

Route::group(array('prefix'=>'api', 'namespace'=>'App\\Modules\\Groups\\Controllers'),function() {
    Route::pattern('id', '[0-9]+');
    Route::get('groups',           array('uses'  =>  'APIController@actionIndex'));
    Route::get('groups/{id}',      array('uses'  =>  'APIController@actionView'));
    Route::post('groups',          array('uses'  =>  'APIController@actionCreate'));
    Route::put('groups/{id}',      array('uses'  =>  'APIController@actionUpdate'));
    Route::delete('groups/{id?}',      array('uses'  =>  'APIController@actionDelete'));

});