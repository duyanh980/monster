<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Groups extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('groups', function(Blueprint $table)
		{
            $table->increments('group_id');
            $table->string('group_name',50)->unique();
            $table->integer('user_id');
            $table->timestamps();

            Schema::create('permissions', function(Blueprint $table)
            {
                $table->increments('permission_id');
                $table->string('permission_name',250)->unique();
                $table->text('permission_description')->nulable();
                $table->integer('user_id');
                $table->timestamps();

                Schema::create('group_permission', function(Blueprint $table)
                {
                    $table->integer('permission_id');
                    $table->integer('group_id');
                    $table->primary(array('permission_id','group_id'));
                });
            });
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('groups');
        Schema::drop('permissions');
        Schema::drop('group_permission');
	}

}
