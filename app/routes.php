<?php

/**
 * Product: Monster
 * Author: duyanh980@gmail.com
 */
/*
App::error(function(Exception $exception, $code)
{
    \Monster\Monster::HandlingError($exception,$code);
    if(!Config::get('app.debug')) {
        return View::make('errors/'.$code);
    }
});
*/



//front end
Route::group(array(),function() {
    Route::get('/login', array('as'   =>    'homepage',   'uses'  =>  'FrontendController@getLogin'));
    Route::get('/logout', array('as'   =>    'logout',   'uses'  =>  'FrontendController@getLogout'));
});

//back end
Route::group(array('prefix'=>'admin'),function() {
    Route::get('/', array('as'   =>    'admin.dashboard',   'uses'  =>  'BackendController@getIndex'));
});
