<!DOCTYPE html>
<html data-ng-app="RocketApp" lang="en">
<head>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta charset="utf-8"/>
    <meta content="Monster admin panel" name="description"/>
    <meta content="duyanh980@gmail.com" name="author"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title data-ng-bind="'Monser Admin Panel | ' + $state.current.data.pageTitle"></title>
    {{ HTML::style('assets/plugins/boostrap/css/bootstrap.min.css') }}
    {{ HTML::style('assets/plugins/boostrap/css/bootstrap-theme.min.css') }}
    {{ HTML::style('assets/plugins/bootstrap-select/select.css') }}
    {{ HTML::style('assets/plugins/bootstrap-datepicker/css/datepicker.css') }}
    {{ HTML::style('assets/plugins/font-awesome/css/font-awesome.css') }}
    {{ HTML::style('assets/plugins/jquery-mcustomscroll/jquery.mCustomScrollbar.min.css') }}
    {{ HTML::style('assets/plugins/jquery-datatable/css/jquery.dataTables.css') }}
    {{ HTML::style('assets/plugins/jquery-validate/css/validationEngine.jquery.css') }}
    {{ HTML::style('assets/plugins/jquery-notifications/css/messenger.css') }}
    {{ HTML::style('assets/plugins/jquery-notifications/css/messenger-theme-flat.css') }}

    {{ HTML::style('assets/css/style.css') }}
    {{ HTML::style('assets/css/responsive.css') }}

    <!--[if lt IE 9]>
    {{ HTML::script('assets/js/html5shiv.js') }}
    {{ HTML::script('assets/js/respond.min.js') }}
    <![endif]-->

    <!-- core js -->
    {{ HTML::script('assets/plugins/jquery-1.8.3.min.js') }}
    {{ HTML::script('assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js') }}
    {{ HTML::script('assets/plugins/boostrap/js/bootstrap.min.js') }}
    {{ HTML::script('assets/plugins/bootstrap-select/select.min.js') }}

    <!-- plugin -->
    {{ HTML::script('assets/plugins/breakpoints.js') }}
    {{ HTML::script('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}
    {{ HTML::script('assets/plugins/jquery-unveil/jquery.unveil.min.js') }}
    {{ HTML::script('assets/plugins/jquery-lazyload/jquery.lazyload.min.js') }}
    {{ HTML::script('assets/plugins/jquery-mcustomscroll/jquery.mousewheel.min.js') }}
    {{ HTML::script('assets/plugins/jquery-mcustomscroll/jquery.mCustomScrollbar.min.js') }}
    {{ HTML::script('assets/plugins/jquery-datatable/js/jquery.dataTables.min.js') }}
    {{ HTML::script('assets/plugins/datatables-responsive/js/datatables.responsive.js') }}
    {{ HTML::script('assets/plugins/datatables-responsive/js/lodash.min.js') }}
    {{ HTML::script('assets/plugins/jquery-validate/js/jquery.validationEngine.js') }}
    {{ HTML::script('assets/plugins/jquery-validate/js/jquery.validationEngine-en.js') }}
    {{ HTML::script('assets/plugins/jquery-notifications/js/messenger.min.js') }}
    {{ HTML::script('assets/plugins/jquery-notifications/js/messenger-theme-future.js') }}
    {{ HTML::script('assets/js/datatables.js') }}

    <!-- angular resource -->
    {{ HTML::script('assets/plugins/angular/angular.min.js') }}
    {{ HTML::script('assets/plugins/angular/plugins/angular-ui-router.min.js') }}
    {{ HTML::script('assets/plugins/angular/plugins/ui-bootstrap-tpls.min.js') }}
    {{ HTML::script('assets/plugins/angular/plugins/ocLazyLoad.min.js') }}
    <!-- angular plugin -->
    {{ HTML::script('assets/plugins/jquery-datatable/extra/js/angular-datatables.min.js') }}

    <!-- run script -->
    {{ HTML::script('assets/js/core.js') }}
    {{ HTML::script('app/admin.js') }}
    {{ HTML::script('app/admin/services/UserService.js') }}

</head>
<body data-ng-controller="AppController" class="page-on-load">

    <div id="preloader" data-ng-spinner-bar>
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
    </div>

    <div data-ng-include="'views/admin/header.html'" data-ng-controller="HeaderController" class="header navbar navbar-inverse"></div>
    <div class="page-container row-fluid">
        <div data-ng-include="'views/admin/sidebar.html'" data-ng-controller="SidebarController" class="page-sidebar" id="main-menu">
        </div>

        <div class="page-content">
            <div class="content" data-ui-view>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</body>
</html>
