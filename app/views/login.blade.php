<!DOCTYPE html>
<html data-ng-app="LoginApp" lang="en">
<head>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta charset="utf-8"/>
    <meta content="Monster admin panel" name="description"/>
    <meta content="duyanh980@gmail.com" name="author"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title data-ng-bind="'Monser Admin Panel | ' + $state.current.data.pageTitle"></title>
    {{ HTML::style('assets/plugins/boostrap/css/bootstrap.min.css') }}
    {{ HTML::style('assets/plugins/boostrap/css/bootstrap-theme.min.css') }}
    {{ HTML::style('assets/plugins/font-awesome/css/font-awesome.css') }}

    {{ HTML::style('assets/css/style.css') }}
    {{ HTML::style('assets/css/responsive.css') }}

    <!--[if lt IE 9]>
    {{ HTML::script('assets/js/html5shiv.js') }}
    {{ HTML::script('assets/js/respond.min.js') }}
    <![endif]-->

    <!-- core js -->
    {{ HTML::script('assets/plugins/jquery-1.8.3.min.js') }}
    {{ HTML::script('assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js') }}
    {{ HTML::script('assets/plugins/boostrap/js/bootstrap.min.js') }}

    <!-- plugin -->
    {{ HTML::script('assets/plugins/breakpoints.js') }}

    <!-- angular resource -->
    {{ HTML::script('assets/plugins/angular/angular.min.js') }}
    {{ HTML::script('assets/plugins/angular/plugins/angular-ui-router.min.js') }}
    {{ HTML::script('assets/plugins/angular/plugins/ui-bootstrap-tpls.min.js') }}
    {{ HTML::script('assets/plugins/angular/plugins/ocLazyLoad.min.js') }}

    <!-- run script -->
    {{ HTML::script('assets/js/core.js') }}
    {{ HTML::script('app/login.js') }}
</head>
<body data-ng-controller="UserController" class="page-on-load login-page">

<div id="preloader" data-ng-spinner-bar>
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<div class="login-container">
    <form id="login-form" class="login-form" data-ng-submit="login()">
        <div class="grid simple horizontal green">
            <div class="grid-title">
                <h4>Sign in to <span class="semi-bold">Monster</span></h4>
            </div>
            <div class="grid-body">
                <div class="row">
                    <div class="alert alert-error" data-ng-show="ErrorMessage">
                    <button data-dismiss="alert" class="close"></button>
                        Email or password incorrect
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12">
                        <label class="form-label">Username</label>

                        <div class="controls">
                            <div class="input-with-icon  right">
                                <input type="email" class="form-control"
                                       data-ng-model="UserData.email" required />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12">
                        <label class="form-label">Password</label>
                        <span class="help"></span>

                        <div class="controls">
                            <div class="input-with-icon right">
                                <input type="password" class="form-control"
                                       data-ng-model="UserData.password" required/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12">
                        <div class="checkbox checkbox check-success"><a href="#">Trouble login in?</a>&nbsp;&nbsp;
                            <input type="checkbox" id="checkbox1" value="1" data-ng-model="UserData.remember">
                            <label for="checkbox1">Keep me reminded </label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <button class="btn btn-primary btn-cons pull-right" type="submit">Login</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
</body>
</html>
